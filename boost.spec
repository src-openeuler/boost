%define version_enc %(echo "%{version}"|tr \. _)

Name:           boost
Version:        1.83.0
Release:        6
Summary:        The free peer-reviewed portable C++ source libraries
License:        BSL-1.0
URL:            https://www.boost.org
Source0:        https://archives.boost.io/release/%{version}/source/boost_%{version_enc}.tar.bz2

# These following patches have been merged into boost develop branch
# Remove them at the next release
# https://github.com/boostorg/graph/pull/318
Patch1: boost-1.81-graph-Dont-run-performance-test-in-CI.patch
# https://github.com/boostorg/random/pull/83
Patch2: boost-1.81-random-Update-multiprecision_float_test.cpp-to-not-overflow.patch
Patch3: boost-1.81-random-Update-multiprecision_int_test.cpp-to-not-accidental.patch
Patch4: boost-1.81-random-test-Add-missing-includes.patch
# https://github.com/boostorg/phoenix/pull/116
Patch5: boost-1.81-phoenix-Update-avoid-placeholders-uarg1.10-ODR-violates.patch
# https://lists.boost.org/Archives/boost/2020/04/248812.php
Patch6: boost-1.73.0-cmakedir.patch

Requires:       %{name}-atomic%{?_isa} = %{version}-%{release}
Requires:       %{name}-chrono%{?_isa} = %{version}-%{release}
Requires:       %{name}-container%{?_isa} = %{version}-%{release}
Requires:       %{name}-context%{?_isa} = %{version}-%{release}
Requires:       %{name}-contract%{?_isa} = %{version}-%{release}
Requires:       %{name}-coroutine%{?_isa} = %{version}-%{release}
Requires:       %{name}-date-time%{?_isa} = %{version}-%{release}
Requires:       %{name}-fiber%{?_isa} = %{version}-%{release}
Requires:       %{name}-filesystem%{?_isa} = %{version}-%{release}
Requires:       %{name}-graph%{?_isa} = %{version}-%{release}
Requires:       %{name}-iostreams%{?_isa} = %{version}-%{release}
Requires:       %{name}-json%{?_isa} = %{version}-%{release}
Requires:       %{name}-locale%{?_isa} = %{version}-%{release}
Requires:       %{name}-log%{?_isa} = %{version}-%{release}
Requires:       %{name}-math%{?_isa} = %{version}-%{release}
Requires:       %{name}-nowide%{?_isa} = %{version}-%{release}
Requires:       %{name}-program-options%{?_isa} = %{version}-%{release}
Requires:       %{name}-python3%{?_isa} = %{version}-%{release}
Requires:       %{name}-random%{?_isa} = %{version}-%{release}
Requires:       %{name}-regex%{?_isa} = %{version}-%{release}
Requires:       %{name}-serialization%{?_isa} = %{version}-%{release}
Requires:       %{name}-stacktrace%{?_isa} = %{version}-%{release}
Requires:       %{name}-system%{?_isa} = %{version}-%{release}
Requires:       %{name}-test%{?_isa} = %{version}-%{release}
Requires:       %{name}-thread%{?_isa} = %{version}-%{release}
Requires:       %{name}-timer%{?_isa} = %{version}-%{release}
Requires:       %{name}-type_erasure%{?_isa} = %{version}-%{release}
Requires:       %{name}-url%{?_isa} = %{version}-%{release}
Requires:       %{name}-wave%{?_isa} = %{version}-%{release}

BuildRequires:  gcc-c++
# required by python3
BuildRequires:  python3-devel python3-unversioned-command
# optional library for asio
BuildRequires:  openssl-devel
# optional libraries for iostreams
BuildRequires:  bzip2-devel libzstd-devel xz-devel zlib-devel
%ifnarch loongarch64 riscv64 sw_64 mips64el
# optional library for multiprecision
BuildRequires:  libquadmath-devel libquadmath-static
%endif
# optional library for python3
BuildRequires:  python3-numpy
# optional library for regex
BuildRequires:  libicu-devel
BuildRequires:  cmake-rpm-macros

Obsoletes: boost-signals < 1.69.0

%description
Boost provides free peer-reviewed portable C++ source libraries.

We emphasize libraries that work well with the C++ Standard Library.
Boost libraries are intended to be widely useful, and usable across
a broad spectrum of applications. The Boost license encourages
the use of Boost libraries for all users with minimal restrictions.

%package atomic
Summary: Run-time component for boost atomic.

%description atomic
C++11-style atomic<>.

%package chrono
Summary: Run-time component for boost chrono.

%description chrono
Useful time utilities.

%package container
Summary: Run-time component for boost container.

%description container
Standard library containers and extensions.

%package context
Summary: Run-time component for boost context.

%description context
(C++11) Context switching library.

%package contract
Summary: Run-time component for boost contract.

%description contract
Contract programming for C++.

%package coroutine
Summary: Run-time component for boost coroutine.

%description coroutine
(deprecated) Coroutine library.

%package date-time
Summary: Run-time component for boost date-time.

%description date-time
A set of date-time libraries based on generic programming concepts.

%package fiber
Summary: Run-time component for boost fiber.

%description fiber
(C++11) Userland threads library.

%package filesystem
Summary: Run-time component for boost filesystem.

%description filesystem
Portable facilities to query and manipulate paths, files, and directories.

%package graph
Summary: Run-time component for boost graph.
Requires: boost-regex%{?_isa} = %{version}-%{release}

%description graph
The BGL graph interface and graph components are generic.

%package iostreams
Summary: Run-time component for boost iostreams.

%description iostreams
A framework for defining streams, stream buffers and i/o filters.

%package json
Summary: Run-time component for boost json.

%description json
JSON parsing, serialization, and DOM in C++11

%package locale
Summary: Run-time component for boost locale.
Requires: boost-chrono%{?_isa} = %{version}-%{release}
Requires: boost-thread%{?_isa} = %{version}-%{release}

%description locale
Provide localization and Unicode handling tools for C++.

%package log
Summary: Run-time component for boost log.

%description log
Logging library.

%package math
Summary: Run-time component for boost math.

%description math
Boost.Math includes several contributions in the domain of mathematics.

%package nowide
Summary: Run-time component for boost nowide.

%description nowide
Standard library functions with UTF-8 API on Windows.

%package program-options
Summary: Run-time component for boost program-options.

%description program-options
The program_options library allows program developers to obtain program options.

%package python3
Summary: Run-time component for boost python.

%description python3
A framework for interfacing Python and C++.

%package random
Summary: Run-time component for boost random.

%description random
A complete system for random number generation.

%package regex
Summary: Run-time component for boost regex.

%description regex
Regular expression library.

%package serialization
Summary: Run-time component for boost serialization.

%description serialization
Serialization for persistence and marshalling.

%package stacktrace
Summary: Run-time component for boost stacktrace.

%description stacktrace
Gather, store, copy and print backtraces.

%package system
Summary: Run-time component for boost system.

%description system
Extensible error reporting.

%package test
Summary: Run-time component for boost test.

%description test
Support for simple program testing, full unit testing, and for program execution monitoring.

%package thread
Summary: Run-time component for boost thread.

%description thread
Portable C++ multi-threading.

%package timer
Summary: Run-time component for boost timer.
Requires: boost-chrono%{?_isa} = %{version}-%{release}

%description timer
Event timer, progress timer, and progress display classes.

%package type_erasure
Summary: Run-time component for boost type-erasure.
Requires: boost-chrono%{?_isa} = %{version}-%{release}

%description type_erasure
Runtime polymorphism based on concepts.

%package url
Summary: Run-time component for boost url.

%description url
A library for parsing, modifying, and printing URLs using only C++11.

%package wave
Summary: Run-time component for boost wave.
Requires: boost-chrono%{?_isa} = %{version}-%{release}
Requires: boost-filesystem%{?_isa} = %{version}-%{release}
Requires: boost-thread%{?_isa} = %{version}-%{release}

%description wave
An implementation of the mandated C99/C++ preprocessor functionality.

%package devel
Summary: The Boost C++ headers, shared and static development libraries
Requires: boost%{?_isa} = %{version}-%{release}
Provides: boost-static
Obsoletes: boost-static < %{version}

%description devel
Headers shared object symbolic links for the Boost C++ libraries and static
Boost C++ libraries distributed with boost.

%package help
Summary: HTML documentation and source code example for the Boost C++ libraries
BuildArch: noarch
Requires: boost-devel = %{version}-%{release}

%description help
This package contains the documentation in the HTML format and source code examples
of the Boost C++ libraries. The documentation provides the same content
as that on the Boost web page (http://www.boost.org/doc/libs/%{version_enc}).

%prep
%autosetup -p1 -n %{name}_%{version_enc}

%build
./bootstrap.sh --with-toolset=%{__cc}
%define opt_build -d+2 -q %{?_smp_mflags}
%define opt_feature release debug-symbols=on pch=off
%define opt_libs --without-mpi --without-graph_parallel
%define b2_options %{opt_build} %{opt_feature} %{opt_libs}
%define boost_macros define=BOOST_NO_AUTO_PTR define=BOOST_STATECHART_USE_NATIVE_RTTI
./b2 %{b2_options} cxxflags="$RPM_OPT_FLAGS" %{boost_macros}

%check
(cd status
 ../b2 %{?_smp_mflags} --check-libs-only
)

%install
./b2 %{b2_options} \
  --prefix=$RPM_BUILD_ROOT%{_prefix} \
  --libdir=$RPM_BUILD_ROOT%{_libdir} \
  install

# Remove buildroot definition from cmake files
sed -i -e 's,%{buildroot},,g' $(find %{buildroot}%{_libdir}/cmake -name "*.cmake")

mkdir boost-doc boost-example
cp -r index.html boost.png rst.css boost.css doc more boost-doc
find libs -name doc -exec cp {} boost-doc --parents -r \;
find libs -name example -exec cp {} boost-example --parents -r \;

%files
%license LICENSE_1_0.txt

%files atomic
%license LICENSE_1_0.txt
%{_libdir}/libboost_atomic.so.%{version}

%files chrono
%license LICENSE_1_0.txt
%{_libdir}/libboost_chrono.so.%{version}

%files container
%license LICENSE_1_0.txt
%{_libdir}/libboost_container.so.%{version}

%files context
%license LICENSE_1_0.txt
%{_libdir}/libboost_context.so.%{version}

%files contract
%license LICENSE_1_0.txt
%{_libdir}/libboost_contract.so.%{version}

%files coroutine
%license LICENSE_1_0.txt
%{_libdir}/libboost_coroutine.so.%{version}

%files date-time
%license LICENSE_1_0.txt
%{_libdir}/libboost_date_time.so.%{version}

%files fiber
%license LICENSE_1_0.txt
%{_libdir}/libboost_fiber.so.%{version}

%files filesystem
%license LICENSE_1_0.txt
%{_libdir}/libboost_filesystem.so.%{version}

%files graph
%license LICENSE_1_0.txt
%{_libdir}/libboost_graph.so.%{version}

%files iostreams
%license LICENSE_1_0.txt
%{_libdir}/libboost_iostreams.so.%{version}

%files json
%license LICENSE_1_0.txt
%{_libdir}/libboost_json.so.%{version}

%files locale
%license LICENSE_1_0.txt
%{_libdir}/libboost_locale.so.%{version}

%files log
%license LICENSE_1_0.txt
%{_libdir}/libboost_log.so.%{version}
%{_libdir}/libboost_log_setup.so.%{version}

%files math
%license LICENSE_1_0.txt
%{_libdir}/libboost_math_c99.so.%{version}
%{_libdir}/libboost_math_c99f.so.%{version}
%ifnarch ppc64le
%{_libdir}/libboost_math_c99l.so.%{version}
%endif
%{_libdir}/libboost_math_tr1.so.%{version}
%{_libdir}/libboost_math_tr1f.so.%{version}
%ifnarch ppc64le
%{_libdir}/libboost_math_tr1l.so.%{version}
%endif

%files nowide
%license LICENSE_1_0.txt
%{_libdir}/libboost_nowide.so.%{version}

%files program-options
%license LICENSE_1_0.txt
%{_libdir}/libboost_program_options.so.%{version}

%files python3
%license LICENSE_1_0.txt
%{_libdir}/libboost_python%{python3_version_nodots}.so.%{version}
%{_libdir}/libboost_numpy%{python3_version_nodots}.so.%{version}

%files random
%license LICENSE_1_0.txt
%{_libdir}/libboost_random.so.%{version}

%files regex
%license LICENSE_1_0.txt
%{_libdir}/libboost_regex.so.%{version}

%files serialization
%license LICENSE_1_0.txt
%{_libdir}/libboost_serialization.so.%{version}
%{_libdir}/libboost_wserialization.so.%{version}

%files stacktrace
%license LICENSE_1_0.txt
%{_libdir}/libboost_stacktrace_addr2line.so.%{version}
%{_libdir}/libboost_stacktrace_basic.so.%{version}
%{_libdir}/libboost_stacktrace_noop.so.%{version}

%files system
%license LICENSE_1_0.txt
%{_libdir}/libboost_system.so.%{version}

%files test
%license LICENSE_1_0.txt
%{_libdir}/libboost_prg_exec_monitor.so.%{version}
%{_libdir}/libboost_unit_test_framework.so.%{version}

%files thread
%license LICENSE_1_0.txt
%{_libdir}/libboost_thread.so.%{version}

%files timer
%license LICENSE_1_0.txt
%{_libdir}/libboost_timer.so.%{version}

%files type_erasure
%license LICENSE_1_0.txt
%{_libdir}/libboost_type_erasure.so.%{version}

%files url
%license LICENSE_1_0.txt
%{_libdir}/libboost_url.so.%{version}

%files wave
%license LICENSE_1_0.txt
%{_libdir}/libboost_wave.so.%{version}

%files help
%license LICENSE_1_0.txt
%doc boost-doc boost-example

%files devel
%license LICENSE_1_0.txt
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/cmake/*

%changelog
* Thu Mar 06 2025 mahailiang <mahailiang@uniontech.com> - 1.83.0-6
- Disable libquadmath dependency for sw_64 mips64el

* Sat Nov 30 2024 Funda Wang <fundawang@yeah.net> - 1.83.0-5
- install cmake config files as suggested by cmake

* Sun Apr 28 2024 yinsist <jianhui.oerv@isrc.iscas.ac.cn> - 1.83.0-4
- Disable libquadmath dependency for RISC-V as gcc does not currently support RISC-V's libquadmath

* Thu Mar 14 2024 jammyjellyfish <jammyjellyfish255@outlook.com> - 1.83.0-3
- Support specify CC

* Sat Mar  2 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 1.83.0-2
- No libquadmath-devel libquadmath-static on loongarch64

* Thu Jan 25 2024 chenhaixiang <chenhaixiang3@huawei.com> - 1.83.0-1
- Update to 1.83.0

* Mon Nov 27 2023 jiahua.yu <jiahua.yu@shingroup.cn> - 1.81.0-3
- Init support for ppc64le

* Mon Sep 04 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 1.81.0-2
- Avoid boost::phoenix::placeholders::uarg1..10 ODR violations

* Mon Dec 19 2022 Liu Zixian <liuzixian4@huawei.com> - 1.81.0-1
- Update to 1.81.0

* Thu Dec 08 2022 Liu Zixian <liuzixian4@huawei.com> - 1.80.0-5
- statechart: avoid undefined behavior

* Fri Dec 02 2022 Liu Zixian <liuzixian4@huawei.com> - 1.80.0-4
- Skip gcc value init check

* Sat Nov 26 2022 Liu Zixian <liuzixian4@huawei.com> - 1.80.0-3
- Fix libquadmath dependency

* Sat Nov 26 2022 Liu Zixian <liuzixian4@huawei.com> - 1.80.0-2
- Backport outcome regression test bugfix

* Wed Aug 24 2022 Liu Zixian <liuzixian4@huawei.com> - 1.80.0-1
- update to 1.80.0

* Tue Jun 07 2022 liukuo <liukuo@kylinos.cn> - 1.79.0-3
- License compliance rectification

* Fri May 27 2022 Liu Zixian <liuzixian4@huawei.com> - 1.79.0-2
- enable %check

* Sat Apr 16 2022 Liu Zixian <liuzixian4@huawei.com> - 1.79.0-1
- update to 1.79.0

* Sun Mar 20 2022 Liu Zixian <liuzixian4@huawei.com> - 1.78.0-12
- backport upstream filesystem patches to fix a possible security issue

* Wed Mar 02 2022 Liu Zixian <liuzixian4@huawei.com> - 1.78.0-11
- fix boost-example, only libs/*/example should go into boost-example.

* Thu Feb 03 2022 Liu Zixian <liuzixian4@huawei.com> - 1.78.0-10
- disable auto_ptr which is warned by -Wdeprecated-declarations
- Replaced deprecated python API

* Sat Jan 29 2022 Liu Zixian <liuzixian4@huawei.com> - 1.78.0-9
- simplify building scripts

* Fri Jan 28 2022 Liu Zixian <liuzixian4@huawei.com> - 1.78.0-8
- backport bugfix for boost 1.78.0 known issues
- https://www.boost.org/users/history/version_1_78_0.html

* Sun Jan 23 2022 Liu Zixian <liuzixian4@huawei.com> - 1.78.0-7
- refactor boost-help

* Fri Dec 31 2021 sdlzx <sdlzx@163.com> - 1.78.0-6
- Don't package boost build tools

* Mon Dec 27 2021 sdlzx <sdlzx@163.com> - 1.78.0-5
- Remove mpi related scripts

* Fri Dec 24 2021 sdlzx <sdlzx@163.com> - 1.78.0-4
- Update package info
- Reference: https://www.boost.org/doc/libs/1_78_0/

* Sun Dec 19 2021 sdlzx <sdlzx@163.com> - 1.78.0-3
- Simplify spec: extract common b2 configs into b2_config

* Fri Dec 10 2021 sdlzx <sdlzx@163.com> - 1.78.0-2
- Simplify spec: remove useless bcond

* Fri Dec 10 2021 sdlzx <sdlzx@163.com> - 1.78.0-1
- update to 1.78.0

* Wed Oct 06 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-4
- Remove dependencies on header-only libraries

* Sun Sep 26 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-3
- Remove linking script which is fixed in upstream PR266

* Wed Sep 22 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-2
- Correct license name for ci.

* Wed Sep 15 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-1
- update to 1.77.0

* Sat Jul 24 2021 Liu Zixian <liuzixian4@huawei.com> - 1.76.0-1
- update to 1.76.0

* Sat Jul 24 2021 Liu Zixian <liuzixian4@huawei.com> - 1.75.0-6
- remove needless BuildRequires

* Fri Jul 23 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.75.0-5
- remove useless buildrequires

* Wed Jun 23 2021 Xu Huijie <xuhuijie2@huawei.com> - 1.75.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix dead loop in parse_repeat()

* Fri Feb 5 2021 xinghe <xinghe1@huawei.com> - 1.75.0-3
- fix the conflict between the devel package file and the other subpackage file

* Thu Feb 4 2021 xinghe <xinghe1@huawei.com> - 1.75.0-2
- fix master build

* Wed Feb 3 2021 xinghe <xinghe1@huawei.com> - 1.75.0-1
- update version to 1.75.0

* Wed Dec 16 2020 xinghe <xinghe1@huawei.com> - 1.73.0-2
- correct license

* Fri Jul 24 2020 Wang Shuo<wangshuo47@huawei.com> - 1.73.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update boost to 1.73.0

* Mon Apr 20 2020 Wang Shuo<wangshuo47@huawei.com> - 1.72.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update boost to 1.72.0

* Thu Mar 19 2020 Yu Xiangyang<yuxiangyang4@huawei.com> - 1.66.0-18
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix build src.rpm error

* Tue Mar 10 2020 Wang Shuo<wangshuo47@huawei.com> - 1.66.0-17
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: move examples files to examples package

* Mon Oct 28 2019 caomeng <caomeng5@huawei.com> - 1.66.0-16
- Type:NA
- ID:NA
- SUG:NA
- DESC:add bcondwith openmpi and mpich

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.66.0-15
- Package init
